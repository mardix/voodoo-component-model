<?php
/** Voodoo\Component
 ******************************************************************************
 * @desc        Extends Voodoo\Model by adding some extra ORM feature in it
 * @package     Voodoo\Component\Model
 * @name        BaseModel
 * @copyright   (c) 2014
 ******************************************************************************/

namespace Voodoo\Component\Model;

use Voodoo,
    Closure;

abstract class BaseModel  extends Voodoo\Core\Model
{

    /**
     * $timestampable 
     * It allows the object to update the datetime_created and datetime_updated
     * on insert and update
     * @var bool
     */
    private $timestampable = true;
    private $createdAtColumnName = "created_at";
    private $updatedAtColumnName = "updated_at";
    private $tablePrefix = "";
    private $callbacks = [
        "onInsert" => null,
        "onUpdate" => null,
        "onDelete" => null
    ];
 /*******************************************************************************/

    public function __construct(\PDO $pdo = null) {
        parent::__construct($pdo);
        $this->setup();      
    }
    
    /**
     * Setup
     * Add logic before doing anything
     */
    protected function setup()
    {
        /**
         * Setup the table if it doesn't exists
         */
        $this->setupTable();
        $this->reset();   

    }
    
    /**
     * Allow to setup the table. It can be create, update etc...
     * Execute the query with :
     *              $this->query($sql);
     * To create the table, you may do $this
     */
    protected function setupTable()
    {}
    
    /**
     * To create a table
     * @param string|Array $sql. If array, it will build it
     */
    final protected function createTable($sql, $primaryKey = null, Array $indexes = [])
    {
        if (! $this->tableExists()) {
            $this->query($sql);
        }
    }
    
    /**
     * Set the table prefix. Which will be removed when doing an alias
     * 
     * @param string $prefix
     * @return \Voodoo\Component\Model\Model
     */
    protected function setTablePrefix($prefix) 
    {
        $this->tablePrefix = $prefix;
        return $this;
    }
    
    /**
     * Set if the table is timestampable. If true, time will be updated automatically
     * @param type $bool
     * @return \Voodoo\Component\Model\Model
     */
    protected function timestampable($bool = true) 
    {
        $this->timestampable = $bool;
        return $this;
    }
    
    /**
     * To set a created_at column name
     * 
     * @param string $columnName
     * @return \Voodoo\Component\Model\Model
     */
    protected function setCreatedAtColumnName($columnName) 
    {
        $this->createdAtColumnName = $columnName; 
        return $this;
    }
    
      /**
     * To set a updated_at column name
     * @param string $columnName
     * @return \Voodoo\Component\Model\Model
     */
    protected function setUpdatedAtColumnName($columnName) 
    {
        $this->updatedAtColumnName = $columnName; 
        return $this;
    }
    
    /**
     * Setup a callback to execute on Insert
     * @param \Closure $callback
     * @return \Voodoo\Component\Model\BaseModel
     */
    protected function onInsert(Closure $callback = null)
    {
        $this->callbacks["onInsert"] = $callback;
        return $this;
    }

    /**
     * Setup a callback to execute on Update
     * @param \Closure $callback
     * @return \Voodoo\Component\Model\BaseModel
     */
    protected function onUpdate(Closure $callback = null)
    {
        $this->callbacks["onUpdate"] = $callback;
        return $this;
    }

    /**
     * Setup a callback to execute on Delete
     * @param \Closure $callback
     * @return \Voodoo\Component\Model\BaseModel
     */
    protected function onDelete(Closure $callback = null)
    {
        $this->callbacks["onDelete"] = $callback;
        return $this;
    }    
    
    /**
     * Reformat PK to remove prefix
     * @return string
     */
    public function getPrimaryKeyname() 
    {
        $pk = parent::getPrimaryKeyname();
        return preg_replace("/^{$this->tablePrefix}/","", $pk);
    }
    
    /**
     * Reformat FK to remove prefix
     * @return string
     */
    public function getForeignKeyname()
    {
        $fk = parent::getForeignKeyname();
        return preg_replace("/^{$this->tablePrefix}/","", $fk);
    } 
    
    
    /**
     * Return the table name without the prefix
     * @return string
     */
    public function tableName()
    {
        return preg_replace("/^{$this->tablePrefix}/", "", $this->getTablename());
    }   
    
    
    /***
     * Check if the table exists
     * @return bool
     */
    protected function tableExists() 
    {
        $res = $this->query("SHOW TABLES LIKE '{$this->getTableName()}'");
        $res = ($res->rowCount() > 0) ? true : false;
        $this->reset();
        return $res;
    } 
/*******************************************************************************/
# Entry display
    
    /**
     * Set the display to 1 for an item
     * @param bool $bool
     * @return \Voodoo\Component\Model\BaseModel
     */
    protected function setDisplay($bool = true)
    {
        if ($this->isSingleRow()) {
            $column = $this->getTableAlias().".display";
            $this->update([$column => $bool ? 1 : 0]);
        }
        return $this;
    }

    /**
     * Checks if an entry is hidden or not
     * @return Bool
     */
    protected function isDisplayed()
    {
        return ($this->display == 1) ? false : true;
    } 
    
    /**
     * Select where display is 1
     * @param bool $bool
     * @return \Voodoo\Component\Model\BaseModel
     */
    protected function whereDisplayed($bool = true)
    {
        $column = $this->getTableAlias().".display";
        return $this->where([$column => $bool ? 1 : 0]);
    }

/*******************************************************************************/
// VIEWMODEL
    /**
     * Access a Model's view at the Model's root level.
     * All model classes must be under 'Model\' namespace.
     * It will automatically access the VM from Model\View
     * 
     * i.e: 
     * Class \MyNameSpace\Model\Audio
     * VM: \MyNameSpace\Model\View\Audio
     * 
     * Class \MyNS\Model\Deep\Down\Class
     * VM: \MyNS\Model\View\Deep\Down\Class
     * 
     * Call the view model:\
     * 
     * $vm = (NS\Model\Class)->findOne()->getViewModel();
     * 
     * @return ViewModel
     */
    public function getViewModel()
    {
        $cldClass = get_called_class();
        $xNamespace = preg_split("/\\\?Model\\\/", $cldClass);
        $viewModelClass = implode("\\", [$xNamespace[0],"Model", "View", $xNamespace[1]]);
        return (new $viewModelClass)->get($this);
    }    
/*******************************************************************************/

// JOINABLE QUERY   
    
    /**
     * To join $this query with a pre-made query from another model
     * It is used a DRY method
     * @param \Lib\Model\Model $model
     * @param string $onForeignKey - The on foreign key ie: beat.account_id
     * @param string $join_operator
     * @return \Lib\Model\Model
     * 

     */
    /**
     * To left join $this query with a pre-made query from another model
     * It is used a DRY method. The child Model must have joinAtQuery method 
     * which will do all the necessary stuff
     * 
     * @param \Lib\Model\Model $model
     * @param bool $oneToOneRelationship - By default, it joins on the child foreign an primary key
     *                                  if true, it will join on itself foreign key an primay key
     *                                  when joins on itself with FK and PK, it does a one to one relationship
     * @return \Lib\Model\Model
     * 
     *  Use case: For Account and Beat, a beat can joinAt Account to return the account info
     * <code>
     *  $beat->setTableAlias("beat")
     *      ->select("beat.title")
     *      ->where()->whereIn()
     *      ->joinAt(new Account)
     *      ->orderBy()...
     *      
     * </code>
     */
    final public function joinAt(BaseModel $model, $constraint = "", $oneToOneRelationship = false) {
        $this->setAlias();
        if(! $model->getTableAlias()) {
            $model->setAlias();
        }
        
        if($constraint) {
            $constraint .= " AND ";
        }
        
        if ($oneToOneRelationship) {
            $constraint .= $this->getTableAlias().".".$this->getPrimaryKeyname();
            $constraint .= " = ";
            $constraint .= $model->getTableAlias().".".$this->getForeignKeyname();            
        } else {
            $constraint .= $this->getTableAlias().".".$model->getForeignKeyname();
            $constraint .= " = ";
            $constraint .= $model->getTableAlias().".".$model->getPrimaryKeyname();
        }
        $this->join($model, $constraint, $model->getTableAlias(), "LEFT");
        $model->joinAtQuery($this);
        return $this;
    }
    
    /**
     * joinAtQuery is pre-made query that join the model query with another model
     * It used as a DRY method
     * Create this method in your class to use joinAt
     *
     * @param \Lib\Model\Model $model
     * @throws Exception
     */
    public function joinAtQuery(BaseModel $model)
    {
        throw new Exception(__METHOD__." is undefined");
    }
    

/*******************************************************************************/
// TIMESTAMPABLE  
// Will always inject time when inserting/updating entry
    
    /**
     * To insert
     * @param array $data
     * @return Model
     */
    public function insert(array $data) {
        if($this->timestampable) {
            // check if the data is multi dimention for bulk insert
            $multi = (count($data) != count($data,COUNT_RECURSIVE));   
            if ($multi) {
                $nData = [];
                foreach($data as $dd) {
                    $nData[] = array_merge([
                                $this->updatedAtColumnName => self::NOW(),
                                $this->createdAtColumnName => self::NOW()
                                ], $dd);
                }
                $data = $nData;
            } else {
                    $data = array_merge([
                                $this->createdAtColumnName => self::NOW(),
                                $this->updatedAtColumnName => self::NOW()
                                ], $data);                
            }
        }  

        return parent::insert($this->onCallable("onInsert", $data));
    }
    
    /**
     * To update
     * @param array $data
     * @return int
     */
    public function update(array $data = null) {
        if ($this->timestampable) {
            $this->set($this->updatedAtColumnName, self::NOW());
        }
        return parent::update($this->onCallable("onUpdate", $data));
    }
/*******************************************************************************/    
    /**
     * Set the table alias
     */
    private function setAlias()
    {
        if(! $this->getTableAlias()) {
            $this->setTableAlias($this->tableName());
        }    
    }
    

    /**
     * To execute the callback
     * 
     * @param string $fn - The callback key
     * @param mixed $data
     * @return mixed
     */
    private function onCallable($fn, $data = null)
    {
        if (is_callable($this->callbacks[$fn])) {
            return $this->callbacks[$fn]($data);
        } else {
            return $data;
        }
    }
    
}
