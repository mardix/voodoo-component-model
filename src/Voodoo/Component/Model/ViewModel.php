<?php
/** Voodoo\Component
 ******************************************************************************
 * @desc        An abstract class to define what's in 
 * @package     Voodoo\Component\Model
 * @name        Model
 * @copyright   (c) 2013
 ******************************************************************************/

namespace Voodoo\Component\Model;

use Voodoo,
    DateTime;

abstract class ViewModel 
{
    static private $dateFormat = "D, M d Y";
    
    /**
     * Require classes to have get()
     */
    abstract public function get($model);

    /**
     * Set the date format 
     * @param string $format
     */
    public static function setDateFormat($format)
    {
        self::$dateFormat = $format;
    }
    
    /**
     * Convert string to currency
     * @param type $amount
     * @return  string
     */
    public function toCurrency($amount)
    {
       return Voodoo\Core\Helpers::toCurrency($amount); 
    }
    
    /**
     * Format date
     * @param string $date
     * @return string
     */
    public function toDate($date)
    {
        return (new DateTime($date))->format(self::$dateFormat);
    }
    
}
