<?php
/** Voodoo\Component
 ******************************************************************************
 * @desc        Add redis connection
 * @package     Voodoo\Component\Model
 * @name        Redis
 * @copyright   (c) 2014
 ******************************************************************************/

namespace Voodoo\Component\Model;

use Voodoo,
    Redisent;

class Redis extends Redisent\Redis
{
    /**
     * Connect to the Manager using Voodoo ConnectionManager
     * 
     * @param $dbAlias The alias to connect to
     * @return self
     */
    public static function connect($dbAlias)
    {
        $redis = Voodoo\Core\ConnectionManager::connect($dbAlias);
        $dbNumber = Voodoo\Core\Config::DB()->get("{$dbAlias}.dbNumber");
        $redis->select($dbNumber);
        return $redis;
    }
    
    /**
     * Constructor
     * Redis connection depend on Redisent and this Class
     * 
     * @param string $dsn - the connection pass by the connection Manager
     */
    public function __construct($dsn)
    {
        parent::__construct($dsn);
    }
    
    /**
     * Prepare data to be saved
     * @param mixed $value
     * @return string | float
     */
    public function prepareSet($value)
    {
        if (is_numeric($value)) {
            return (float) $value;
        } else {
            return serialize($value);
        }
    }
    
    /**
     * Prepare retrieved data
     * @param string $value
     * @return mixed
     */
    public function prepareGet($value) 
    {
        if (is_numeric($value)) {
            return $value;
        } else if(! $value) {
            return null;
        } else {
            return unserialize($value);
        }        
    }
    
    
    /**
     * SET
     * @param type $key
     * @param type $data
     * @return type
     */
    public function set($key,$data)
    {
        return parent::set($key, $this->prepareSet($data));
    }
    
    /**
     * GET
     * @param type $key
     * @return null
     */
    public function get($key) 
    {
        return $this->prepareGet(parent::get($key));
    }  
    
    /**
     * HSET
     * @param type $key
     * @param type $field
     * @param type $data
     * @return type
     */
    public function hset($key, $field, $data)
    {
        return parent::hset($key, $field, $this->prepareSet($data));
    }
    
    /**
     * HGET
     * @param type $key
     * @param type $field
     * @return null
     */
    public function hget($key, $field) 
    {
        return $this->prepareGet(parent::hget($key, $field));
    }    
    
    
    /**
     * Return Info to array
     * 
     * @return Array
     */
    public function infoToArray()
    {
        
        $data = $this->info();
        $info      = array();
        $infoLines = preg_split('/\r?\n/', $data);

        foreach ($infoLines as $row) {
            @list($k, $v) = explode(':', $row);

            if ($row === '' || !isset($v)) {
                continue;
            }

            if (!preg_match('/^db\d+$/', $k)) {
                if ($k === 'allocation_stats') {
                    $info[$k] = $this->parseInfoAllocationStats($v);
                    continue;
                }

                $info[$k] = $v;
            } else {
                $info[$k] = $this->parseInfoDatabaseStats($v);
            }
        }

        return $info;        
    }
    
    /**
     * Parses the reply buffer and extracts the statistics of each logical DB.
     *
     * @param string $str Reply buffer.
     * @return array
     */
    protected function parseInfoDatabaseStats($str)
    {
        $db = array();

        foreach (explode(',', $str) as $dbvar) {
            list($dbvk, $dbvv) = explode('=', $dbvar);
            $db[trim($dbvk)] = $dbvv;
        }

        return $db;
    }

    /**
     * Parses the reply buffer and extracts the allocation statistics.
     *
     * @param string $str Reply buffer.
     * @return array
     */
    protected function parseInfoAllocationStats($str)
    {
        $stats = array();

        foreach (explode(',', $str) as $kv) {
            @list($size, $objects, $extra) = explode('=', $kv);

            // hack to prevent incorrect values when parsing the >=256 key
            if (isset($extra)) {
                $size = ">=$objects";
                $objects = $extra;
            }

            $stats[$size] = $objects;
        }

        return $stats;
    }    
    
}
